import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let heroes = [
      {id: 1, name: 'Gokú', rute: 'http://imagenpng.com/wp-content/uploads/2015/03/Dragon-Ball-Z-Png.png'},
      {id: 2, name: 'Vegeta', rute: 'http://vignette2.wikia.nocookie.net/dragonballfanon/images/e/e6/Vegeta_4.png/revision/latest?cb=20130805153739&path-prefix=es'},
      {id: 3, name: 'Gohan', rute: 'http://vignette2.wikia.nocookie.net/dragonball/images/6/6c/Gohan.png/revision/20150810183553?path-prefix=es'},
      {id: 4, name: 'Trunks', rute: 'http://vignette1.wikia.nocookie.net/dragonballfanon/images/5/5f/Trunks_by_sbddbz-d68w96w.png/revision/latest?cb=20140906145542&path-prefix=es'}, 
      {id: 5, name: 'Goten', rute: 'http://img00.deviantart.net/f5aa/i/2016/080/f/c/goten__facudibuja_by_facudibuja-d9vyxe8.png'},
      {id: 6, name: 'Piccolo', rute: 'http://vignette3.wikia.nocookie.net/dragonballfanon/images/2/28/Piccolo_Trans..png/revision/latest?cb=20130129160537&path-prefix=es'},
      {id: 7, name: 'Broly', rute: 'http://vignette3.wikia.nocookie.net/dragonballfanon/images/e/ee/Broly.png/revision/latest?cb=20130103195759&path-prefix=es'},
      {id: 8, name: 'Freezer', rute: 'http://vignette3.wikia.nocookie.net/dragonballmultiverse/images/b/b9/Freezer.png/revision/latest?cb=20130715063600&path-prefix=es'},
      {id: 9, name: 'Cell', rute: 'http://vignette1.wikia.nocookie.net/villains/images/b/b8/Cell.png/revision/latest?cb=20130929044604'},
      {id: 10, name: 'Kid Boo', rute: 'http://vignette4.wikia.nocookie.net/dragonball/images/8/88/Kid_buu_render_dbz.png/revision/latest?cb=20150315163348&path-prefix=es'}
    ];
    return {heroes};
  }
}